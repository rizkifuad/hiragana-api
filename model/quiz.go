package model

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type Quiz struct {
	Question string
	Type     string
	Karakter string
	gorm.Model
}

type QuizVMs struct {
	Question string `json:"question"`
	Type     string `json:"type"`
	Karakter string `json:"karakter"`
	Kanji    string `json:"kanji"`
	Points   string `json:"points"`
	ID       int    `json:"id"`
}

type QuizVM struct {
	Question string `json:"question"`
	Type     string `json:"type"`
	Karakter string `json:"karakter"`
	ID       int    `json:"id"`
}

type QuizPost struct {
	ID       int    `json:"id"`
	Question string `json:"question"`
	Type     string `json:"type"`
	Karakter string `json:"karakter"`
}

func (this *Quiz) GetRandomItems() []QuizVMs {
	items := []QuizVMs{}
	db.Model(&Quiz{}).Select("*").Order("rand()").Limit(5).Scan(&items)

	for key, q := range items {
		kar := Katakana{}
		db.Table(q.Type).Select("*").Where("romawi = ?", q.Karakter).Scan(&kar)
		items[key].Points = kar.Points
		items[key].Kanji = kar.Kanji

	}

	return items
}

func (this *Quiz) GetItems() []QuizVMs {
	items := []QuizVMs{}
	db.Model(&Quiz{}).Select("*").Scan(&items)

	return items
}

type QuizKarakters struct {
	Hiragana []Hiragana `json:"hiragana"`
	Katakana []Katakana `json:"katakana"`
}

func (this *Quiz) GetKarakters() (QuizKarakters, error) {
	kars := QuizKarakters{}
	db.Model(&Katakana{}).Select("romawi").Scan(&kars.Katakana)
	db.Model(&Hiragana{}).Select("romawi").Scan(&kars.Hiragana)
	return kars, nil
}

func (this *Quiz) GetItem(id int) (QuizVM, error) {
	item := QuizVM{}
	var count int
	db.Model(&Quiz{}).Select("*").Where("id = ?", id).Scan(&item).Count(&count)

	//db.Table(item.Type).Model(&Katakana{}).Select("points, romawi").Scan(&karakter)
	//item.Details = karakter
	//answers := []QuizAnswer{}
	//db.Model(&QuizAnswer{}).Select("*").Where("question_id = ?", id).Scan(&answers)

	//item.Answer = answers
	if count == 0 {
		return item, errors.New("Cannot find item")
	}
	return item, nil
}

func (this *QuizPost) Save() error {
	quiz := Quiz{
		Question: this.Question,
		Type:     this.Type,
		Karakter: this.Karakter,
	}

	if this.ID == 0 {
		db.Create(&quiz)
	} else {
		db.Model(&Quiz{}).Where("id = ?", this.ID).Scan(&quiz)
		quiz.Question = this.Question
		quiz.Type = this.Type
		quiz.Karakter = this.Karakter
		db.Save(&quiz)
	}

	return nil
}

func (this *Quiz) Remove() error {
	if this.ID == 0 {
		return errors.New("Cannot delete data")
	}

	db.Delete(&this)
	return nil
}
