package model

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type Hiragana struct {
	Romawi string `json:"romawi"`
	Kanji  string
	Points string `json:"points"`
	gorm.Model
}

type HiraganaVM struct {
	Romawi string `json:"romawi"`
	Kanji  string `json:"kanji"`
	Points string `json:"points"`
	ID     int    `json:"id"`
}

func (this *HiraganaVM) GetItem(romawi string) error {
	db.Model(&Hiragana{}).Select("romawi, kanji, points, id").Where("romawi = ?", romawi).Scan(&this)
	if this.ID == 0 {
		return errors.New("No data found")
	}
	return nil
}

func (this *Hiragana) Save() error {
	if this.ID == 0 {
		db.Create(&this)
	} else {
		db.Save(&this)
	}
	return nil
}
