package model

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type KataKerja struct {
	Title   string
	Flash   string
	Slug    string
	Content string `gorm:"type:TEXT;"`
	Icon    string
	Bagian  int
	gorm.Model
}

type KataKerjaVMs struct {
	Title string `json:"title"`
	Flash string `json:"flash"`
	Slug  string `json:"slug"`
	Icon  string `json:"icon"`
	ID    int    `json:"id"`
}

type KataKerjaVM struct {
	Title   string `json:"title"`
	Flash   string `json:"flash"`
	Slug    string `json:"slug"`
	Content string `json:"content"`
	Icon    string `json:"icon"`
	Bagian  int    `json:"bagian"`
	ID      int    `json:"id"`
}

func (this *KataKerja) GetItems() []KataKerjaVMs {
	items := []KataKerjaVMs{}
	db.Model(&KataKerja{}).Select("icon,title, flash, slug, id").Scan(&items)
	return items
}

func (this *KataKerja) GetItem(id int) (KataKerjaVM, error) {
	item := KataKerjaVM{}
	var count int
	db.Model(&KataKerja{}).Select("title, bagian, content, flash, id, icon, slug").Where("id = ?", id).Scan(&item).Count(&count)
	if count == 0 {
		return item, errors.New("Cannot find item")
	}
	return item, nil
}

func (this *KataKerja) Save() error {
	if this.ID == 0 {
		db.Create(&this)
	} else {
		db.Save(&this)
	}
	return nil
}

func (this *KataKerja) Remove() error {
	if this.ID == 0 {
		return errors.New("Cannot delete data")
	}

	db.Delete(&this)
	return nil
}
