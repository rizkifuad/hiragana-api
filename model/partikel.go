package model

import (
	"errors"

	"github.com/jinzhu/gorm"
)

type Partikel struct {
	Title   string
	Flash   string
	Slug    string
	Content string `gorm:"type:TEXT;"`
	Icon    string
	Bagian  int
	gorm.Model
}

type PartikelVMs struct {
	Title string `json:"title"`
	Flash string `json:"flash"`
	Slug  string `json:"slug"`
	ID    int    `json:"id"`
	Icon  string `json:"icon"`
}

type PartikelVM struct {
	Title   string `json:"title"`
	Flash   string `json:"flash"`
	Slug    string `json:"slug"`
	Content string `json:"content"`
	Icon    string `json:"icon"`
	Bagian  int    `json:"bagian"`
	ID      int    `json:"id"`
}

func (this *Partikel) GetItems() []PartikelVMs {
	items := []PartikelVMs{}
	db.Model(&Partikel{}).Select("title, flash, slug, id, icon").Scan(&items)
	return items
}

func (this *Partikel) GetItem(id int) (PartikelVM, error) {
	item := PartikelVM{}
	var count int
	db.Model(&Partikel{}).Select("title, bagian, content, flash, id, icon, slug").Where("id = ?", id).Scan(&item).Count(&count)
	if count == 0 {
		return item, errors.New("Cannot find item")
	}
	return item, nil
}

func (this *Partikel) Save() error {
	if this.ID == 0 {
		db.Create(&this)
	} else {
		db.Save(&this)
	}
	return nil
}

func (this *Partikel) Remove() error {
	if this.ID == 0 {
		return errors.New("Cannot delete data")
	}

	db.Delete(&this)
	return nil
}
