/* ===========================================================
 * trumbowyg.popup.js v1.0
 * popup plugin for Trumbowyg
 * http://alex-d.github.com/Trumbowyg
 * ===========================================================
 * Author : Casella Edoardo (Civile)
 */


(function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        langs: {
            // jshint camelcase:false
            en: {
                popup: 'Popup'
            },
        },
        // jshint camelcase:true

        plugins: {
            popup: {
                init: function (trumbowyg) {
                    var btnDef = {
                        fn: function () {
                            trumbowyg.saveRange();
                            var text = trumbowyg.getRangeText();
                            if (text.replace(/\s/g, '') !== '') {
                                try {
                                  var container = null;
                                  if (document.selection) { //for IE
                                    container = document.selection.createRange().parentElement();
                                  } else {
                                    var select = window.getSelection();
                                    if (select.rangeCount > 0) {
                                      container = select.getRangeAt(0).startContainer.parentNode;
                                    }
                                  }

                                  if (container.classList[0] === 'popup') {
                                    $(container).children().remove()
                                    $(container).contents().unwrap('span')
                                    return
                                  }


                                  var $modal = trumbowyg.openModalInsert(
                                    trumbowyg.lang.popup,

                                    // Fields
                                    {
                                      input: {
                                        type: 'text',
                                        value: trumbowyg.getRangeText()
                                      },
                                      keterangan: {
                                        type: 'text',
                                      },
                                      romanji: {
                                        type: 'text',
                                      },
                                      arti: {
                                        type: 'text',
                                      }
                                    },

                                    // Callback
                                    function (values) {
                                      trumbowyg.closeModal();
                                      var popupStart = '<span class="popup">' + values.input
                                      var popupEnd = '<span class="popuptext myPopup">'
                                      if (values.keterangan) {
                                        popupEnd += values.keterangan
                                      }

                                      if (values.romanji) {
                                        popupEnd += '<br>romanji: ' + values.romanji
                                      }

                                      if (values.arti) {
                                        popupEnd += '<br>arti: ' + values.arti
                                      }
                                      popupEnd += '</span></span>'

                                      var contents = $(container).html()
                                      contents = contents.replace(values.input, popupStart + popupEnd)

                                      $(container).html(contents)
                                    }
                                  );


                                } catch (e) {
                                  console.log(e)
                                }
                            }
                        },
                      tag: 'pre'
                    };

                  trumbowyg.addBtnDef('popup', btnDef);
                }
            }
        }
    });

  /*
   * GetSelectionParentElement
   */
  function getSelectionParentElement() {
    var parentEl = null,
      selection;
    if (window.getSelection) {
      selection = window.getSelection();
      if (selection.rangeCount) {
        parentEl = selection.getRangeAt(0).commonAncestorContainer;
        if (parentEl.nodeType !== 1) {
          parentEl = parentEl.parentNode;
        }
      }
    } else if ((selection = document.selection) && selection.type !== 'Control') {
      parentEl = selection.createRange().parentElement();
    }
    return parentEl;
  }

  /*
   * Strip
   * returns a text without HTML tags
   */
  function strip(html) {
    var tmp = document.createElement('span');
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || '';
  }

  /*
   * UnwrapCode
   * ADD/FIX: to improve, works but can be better
   * "paranoic" solution
   */
  function unwrapCode() {
    var container = null;
    if (document.selection) { //for IE
      container = document.selection.createRange().parentElement();
    } else {
      var select = window.getSelection();
      if (select.rangeCount > 0) {
        container = select.getRangeAt(0).startContainer.parentNode;
      }
    }
    //'paranoic' unwrap
    var ispre = $(container).contents().closest('pre').length;
    var iscode = $(container).contents().closest('code').length;
    if (ispre && iscode) {
      $(container).contents().unwrap('code').unwrap('pre');
    } else if (ispre) {
      $(container).contents().unwrap('pre');
    } else if (iscode) {
      $(container).contents().unwrap('code');
    }
  }

})(jQuery);

