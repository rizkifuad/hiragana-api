new Vue({
  el:"#root",
  components: {
    Multiselect: window.VueMultiselect.default
  },
  delimiters: ['${', '}'],
  data: {
    type: "create",
    title: "",
    flash: "",
    slug: "",
    icon: "",
    content: "",
    bagian: 0,
    id: 0
  },

  created() {
    var link = window.location.href
    var id = link.split('/')
    id = id[id.length - 1]
    if (id === 'add') {
      this.type = 'create'
    } else {
      this.type = 'update'
      var self = this
      axios.get("/api/kata_kerja/" + id)
      .then( (conns) =>  {
        for (key in conns.data.data) {
          self[key] = conns.data.data[key]
        }

        $('#content').trumbowyg(configurations.plugins);
        $('#content').trumbowyg('html', this.content);
      })
    }

  },

  mounted() {
    $('#content').trumbowyg(configurations.plugins);
  },

  methods: {
    onSubmit() {
      this.content = $('#content').trumbowyg('html')
      var run = axios({
        method:"POST",
        url: "/api/kata_kerja",
        data: parseUrlEncoded({
          title: this.title,
          flash: this.flash,
          slug: this.slug,
          icon: this.icon,
          content: this.content,
          bagian: this.bagian,
          id: this.id
        }),
        headers: {"content-type": "application/x-www-form-urlencoded"}
      })

      run.then( (resp) => {
        window.location = "/kata_kerja"
      })

      run.catch( (err, data) => {
        alert("Unexpeted error occured")
        console.log("menggila", err.response.data.message)
      })

    }
  }
})

var parseUrlEncoded = function(data) {
  var params = new URLSearchParams()
  for (key in data) {
    params.append(key, data[key])
  }
  return params
}
