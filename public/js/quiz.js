new Vue({
  el:"#root",
  components: {
    Multiselect: window.VueMultiselect.default
  },
  delimiters: ['${', '}'],
  data: {
    data: []
  },

  created() {
    var self = this
    axios.get("/api/quiz")
      .then( (conns) =>  {
        self.data = conns.data.data
      })
  },

  methods: {
    remove(index) {
      var id = this.data[index].id
      var del = axios({
        method: "DELETE",
        url: "/api/quiz/" + id,
      })

      del.then( (resp) => {
        this.data.splice(index, 1)
      })
    }
  }


})

var parseUrlEncoded = function(data) {
  var params = new URLSearchParams()
  for (key in data) {
    params.append(key, data[key])
  }
  return params
}
