new Vue({
  el:"#root",
  components: {
    Multiselect: window.VueMultiselect.default
  },
  delimiters: ['${', '}'],
  data: {
    type: "create",
    question: "",
    karakter: "",
    answer_type: "",
    karakters: [],
    id: 0
  },

  created() {
    var link = window.location.href
    var id = link.split('/')
    id = id[id.length - 1]

    if (id === 'add') {
      this.type = 'create'
    } else {
      this.type = 'update'
      var self = this
      var temp_kar = []
      axios.get("/api/quiz/" + id)
      .then( (conns) =>  {
        for (key in conns.data.data) {
          if (key == 'type') {
            self.answer_type = conns.data.data[key]
          } else {
            self[key] = conns.data.data[key]
          }
        }

        temp_kar = self.karakter

        return axios.get("/quiz/prepare")
      }).then(function(karakters) {
        self.karakters = karakters.data.data
        self.karakter = temp_kar
        console.log(temp_kar)

        $('#content').trumbowyg(configurations.plugins);
        $('#content').trumbowyg('html', this.content);
      })

    }

  },

  mounted() {
    $('#content').trumbowyg(configurations.plugins);
  },

  methods: {
    onSubmit() {
      this.content = $('#content').trumbowyg('html')
      var run = axios({
        method:"POST",
        url: "/api/quiz",
        data: parseUrlEncoded({
          question: this.question,
          karakter: this.karakter,
          type: this.answer_type,
          id: this.id
        }),
        headers: {"content-type": "application/x-www-form-urlencoded"}
      })

      run.then( (resp) => {
        window.location = "/quiz"
      })

      run.catch( (err, data) => {
        alert("Unexpected error occured")
        console.log("menggila", err.response.data.message)
      })

    }
  },

  computed: {
    filterKarakter() {
      return this.karakters[this.answer_type]
    }
  }
})

var parseUrlEncoded = function(data) {
  var params = new URLSearchParams()
  for (key in data) {
    params.append(key, data[key])
  }
  return params
}
