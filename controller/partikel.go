package controller

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/rizkix/hiragana-api/model"
)

var Partikel = model.Partikel{}

func getPartikel(w http.ResponseWriter, r *http.Request) {
	items := Partikel.GetItems()

	err := APIsuccess(w, items)
	if err != nil {
		log.Println(err.Error())
	}
}

func getPartikelDetail(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	item, err := Partikel.GetItem(id)

	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func deletePartikel(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	item := model.Partikel{}
	item.ID = uint(id)
	err := item.Remove()
	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func routePartikel(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "partikel.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}

func routePartikelForm(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "partikel_form.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}

func postPartikel(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	item := new(model.Partikel)
	err := decoder.Decode(item, r.PostForm)
	if err != nil {
		APIerror(w, err)
		return
	}

	item.Save()

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}
