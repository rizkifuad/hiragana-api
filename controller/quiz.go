package controller

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/rizkix/hiragana-api/model"
)

var Quiz = model.Quiz{}

func getQuiz(w http.ResponseWriter, r *http.Request) {
	items := Quiz.GetItems()

	err := APIsuccess(w, items)
	if err != nil {
		log.Println(err.Error())
	}
}

func getQuizRandom(w http.ResponseWriter, r *http.Request) {
	items := Quiz.GetRandomItems()

	err := APIsuccess(w, items)
	if err != nil {
		log.Println(err.Error())
	}
}

func getQuizDetail(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	item, err := Quiz.GetItem(id)

	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func prepareQuizForm(w http.ResponseWriter, r *http.Request) {
	item, err := Quiz.GetKarakters()

	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func deleteQuiz(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	item := model.Quiz{}
	item.ID = uint(id)
	err := item.Remove()
	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func routeQuiz(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "quiz.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}

func routeQuizForm(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "quiz_form.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}

func postQuiz(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()

	item := new(model.QuizPost)
	err := decoder.Decode(item, r.PostForm)
	if err != nil {
		APIerror(w, err)
		return
	}
	fmt.Printf(" menggila: %v", item)

	item.Save()

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}
