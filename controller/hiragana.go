package controller

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rizkix/hiragana-api/model"
)

func getHiragana(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	romawi, _ := vars["romawi"]
	var item model.HiraganaVM
	err := item.GetItem(romawi)

	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func updateHiragana(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	item := new(model.Hiragana)
	err := decoder.Decode(item, r.PostForm)
	if err != nil {
		APIerror(w, err)
		return
	}

	item.Save()

	var updated model.HiraganaVM
	updated.ID = int(item.ID)
	updated.Romawi = item.Romawi
	updated.Points = item.Points
	updated.Kanji = item.Kanji

	err = APIsuccess(w, updated)
	if err != nil {
		log.Println(err.Error())
	}
}
