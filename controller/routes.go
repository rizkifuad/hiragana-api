package controller

import (
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"github.com/rizkix/hiragana-api/util"
)

var templates *template.Template
var decoder *schema.Decoder

func Init() *mux.Router {
	decoder = schema.NewDecoder()

	r := mux.NewRouter()

	r.HandleFunc("/api/partikel", getPartikel).Methods("GET")
	r.HandleFunc("/api/partikel/{id:[0-9]+}", getPartikelDetail).Methods("GET")
	r.HandleFunc("/api/partikel", postPartikel).Methods("POST")
	r.HandleFunc("/api/partikel/{id:[0-9]+}", deletePartikel).Methods("DELETE")
	r.HandleFunc("/partikel", routePartikel).Methods("GET")
	r.HandleFunc("/partikel/{id:[0-9]+}", routePartikelForm).Methods("GET")
	r.HandleFunc("/partikel/add", routePartikelForm).Methods("GET")

	r.HandleFunc("/api/kata_kerja", getKataKerja).Methods("GET")
	r.HandleFunc("/api/kata_kerja/{id:[0-9]+}", getKataKerjaDetail).Methods("GET")
	r.HandleFunc("/api/kata_kerja", postKataKerja).Methods("POST")
	r.HandleFunc("/api/kata_kerja/{id:[0-9]+}", deleteKataKerja).Methods("DELETE")
	r.HandleFunc("/kata_kerja", routeKataKerja).Methods("GET")
	r.HandleFunc("/kata_kerja/{id:[0-9]+}", routeKataKerjaForm).Methods("GET")
	r.HandleFunc("/kata_kerja/add", routeKataKerjaForm).Methods("GET")

	r.HandleFunc("/api/hiragana/{romawi:[a-zA-Z]+}", getHiragana).Methods("GET")
	r.HandleFunc("/api/hiragana", updateHiragana).Methods("POST")

	r.HandleFunc("/api/katakana/{romawi:[a-zA-Z]+}", getKatakana).Methods("GET")
	r.HandleFunc("/api/katakana", updateKatakana).Methods("POST")

	r.HandleFunc("/canvas", routeCanvas).Methods("GET")
	r.HandleFunc("/", routeKataKerja).Methods("GET")

	r.HandleFunc("/api/quiz", getQuiz).Methods("GET")
	r.HandleFunc("/api/quiz/random", getQuizRandom).Methods("GET")
	r.HandleFunc("/api/quiz/{id:[0-9]+}", getQuizDetail).Methods("GET")
	r.HandleFunc("/api/quiz", postQuiz).Methods("POST")
	r.HandleFunc("/api/quiz/{id:[0-9]+}", deleteQuiz).Methods("DELETE")
	r.HandleFunc("/quiz", routeQuiz).Methods("GET")
	r.HandleFunc("/quiz/{id:[0-9]+}", routeQuizForm).Methods("GET")
	r.HandleFunc("/quiz/add", routeQuizForm).Methods("GET")
	r.HandleFunc("/quiz/prepare", prepareQuizForm).Methods("GET")

	templates, _ = buildTemplates()
	if os.Getenv("ENV") == "DEV" {
		go func() {
			for range time.Tick(300 * time.Millisecond) {
				tc, needUpdate := buildTemplates()
				if needUpdate {
					fmt.Println("Template change detected, updating..")
					templates = tc
				}
			}
		}()
	}

	return r
}

func mainPage(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "main.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}

func renderView(w http.ResponseWriter, r *http.Request, templatePath string, context interface{}) error {
	w.Header().Set("Content-Type", "text/html")

	pusher, ok := w.(http.Pusher)
	if ok {
		publicDir := "public"
		fileList := []string{}
		err := filepath.Walk(publicDir, func(path string, f os.FileInfo, err error) error {
			if !f.IsDir() {
				fileList = append(fileList, path)
			}
			return nil
		})

		if err == nil {
			for _, file := range fileList {
				staticFile := strings.Replace(file, "public", "", -1)
				pusher.Push(staticFile, nil)
			}
		}
	}

	template := templates.Lookup(templatePath)
	if template != nil {
		responseWrite := util.GetResponseWriter(w, r)
		defer responseWrite.Close()
		err := template.Execute(responseWrite, context)
		if err != nil {
			return errors.New("Cannot parse template:" + err.Error())
		} else {
			return nil
		}
	}

	return errors.New("Cannot parse template")

}

var lastMod time.Time = time.Unix(0, 0)

func buildTemplates() (*template.Template, bool) {
	needUpdate := false
	templates := template.New("templates")
	basePath := "views"
	templateDir, _ := os.Open(basePath)
	defer templateDir.Close()

	fileInfos, _ := templateDir.Readdir(-1)
	fileNames := make([]string, len(fileInfos))
	for i, fi := range fileInfos {
		if !fi.IsDir() {
			if fi.ModTime().After(lastMod) {
				lastMod = fi.ModTime()
				needUpdate = true
			}
			fileNames[i] = basePath + "/" + fi.Name()
		}
	}
	if needUpdate {
		templates.ParseFiles(fileNames...)
	}
	return templates, needUpdate
}

type APImsg struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func APIerror(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "application/json")

	context := APImsg{
		Status:  false,
		Message: err.Error(),
		Data:    nil,
	}

	data, _ := json.Marshal(context)

	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(data))

}

func APIsuccess(w http.ResponseWriter, data interface{}) error {
	w.Header().Set("Content-Type", "application/json")

	context := APImsg{
		Status:  true,
		Message: "",
		Data:    data,
	}

	encoded, err := json.Marshal(context)
	if err != nil {
		return errors.New("Cannot parse json: " + err.Error())
	}

	w.Write([]byte(encoded))

	return nil

}

func routeCanvas(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "canvas.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}
