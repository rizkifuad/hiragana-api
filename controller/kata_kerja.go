package controller

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/rizkix/hiragana-api/model"
)

var KataKerja = model.KataKerja{}

func getKataKerja(w http.ResponseWriter, r *http.Request) {
	items := KataKerja.GetItems()

	err := APIsuccess(w, items)
	if err != nil {
		log.Println(err.Error())
	}
}

func getKataKerjaDetail(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	item, err := KataKerja.GetItem(id)

	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func deleteKataKerja(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	item := model.KataKerja{}
	item.ID = uint(id)
	err := item.Remove()
	if err != nil {
		APIerror(w, err)
		return
	}

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}

func routeKataKerja(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "kata_kerja.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}

func routeKataKerjaForm(w http.ResponseWriter, r *http.Request) {
	err := renderView(w, r, "kata_kerja_form.html", nil)
	if err != nil {
		log.Println(err.Error())
	}
}

func postKataKerja(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	item := new(model.KataKerja)
	err := decoder.Decode(item, r.PostForm)
	if err != nil {
		APIerror(w, err)
		return
	}

	item.Save()

	err = APIsuccess(w, item)
	if err != nil {
		log.Println(err.Error())
	}
}
