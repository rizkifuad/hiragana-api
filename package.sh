go get -u github.com/jinzhu/gorm
go get -u github.com/denisenkom/go-mssqldb
go get -u github.com/gorilla/mux
go get -u github.com/gorilla/securecookie
go get -u github.com/gorilla/schema
go get -u github.com/go-sql-driver/mysql
go get -u github.com/joho/godotenv
