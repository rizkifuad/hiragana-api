package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/gorilla/schema"
	"github.com/joho/godotenv"
	"github.com/rizkix/hiragana-api/controller"
	"github.com/rizkix/hiragana-api/model"
	"github.com/rizkix/hiragana-api/util"
)

var decoder *schema.Decoder

var hash string

func xmain() {
	kata := []string{
		"aa", "ii", "uu", "ee", "oo",
		"ka", "ki", "ku", "ke", "ko",
		"sa", "shi", "su", "se", "so",
		"ta", "chi", "tsu", "te", "to",
		"na", "ni", "nu", "ne", "no",
		"ha", "hi", "fu", "he", "ho",
		"ma", "mi", "mu", "me", "mo",
		"ya", "", "yu", "", "yo",
		"ra", "ri", "ru", "re", "ro",
		"wa", "", "n", "", "wo",
	}
	fmt.Println(kata[0])

	//fmt.Println(os.Getwd())
	//out, _ := exec.Command("pwd").Output()
	//fmt.Println(string(out))

	for _, k := range kata {
		fmt.Println(k)

		out, err := exec.Command("wget", "-O", "sound/"+k+".mp3", "https://www.nhk.or.jp/lesson/mp3/syllabary/"+k+".mp3").Output()
		if err != nil {
			fmt.Println(err.Error())
		}
		fmt.Println(out)
	}
}

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Printf("Error main loading env: %v", err.Error)
	}
	rand.Seed(time.Now().UnixNano())
	hash = randSeq(12)

	decoder = schema.NewDecoder()
	fmt.Println("Initializing templates cache...")
	model.Init()

	routes := controller.Init()
	http.Handle("/", routes)

	http.HandleFunc("/libs/", serveResource)
	http.HandleFunc("/css/", serveResource)
	http.HandleFunc("/img/", serveResource)
	http.HandleFunc("/js/", serveResource)

	http_port := os.Getenv("HTTP_PORT")
	https_port := os.Getenv("HTTPS_PORT")

	fmt.Printf("Serving Hiragana API from https://localhost:%s\n", http_port)
	go http.ListenAndServe("0.0.0.0:"+http_port, nil)
	go http.ListenAndServeTLS("0.0.0.0:"+https_port, "tls/cert.pem", "tls/key.pem", nil)

	fmt.Scanln()
}

func serveResource(w http.ResponseWriter, r *http.Request) {
	path := "public" + r.URL.Path
	var contentType string
	if strings.HasSuffix(path, ".css") {
		contentType = "text/css"
	} else if strings.HasSuffix(path, "png") {
		contentType = "image/png"
	} else {
		contentType = "text/plain"
	}

	f, err := os.Open(path)
	if err != nil {
		log.Println(err.Error())
	}

	defer f.Close()
	w.Header().Set("Content-Type", contentType)
	responseWrite := util.GetResponseWriter(w, r)
	defer responseWrite.Close()
	br := bufio.NewReader(f)
	br.WriteTo(responseWrite)

}
